# shopping-list

## Environment & Setup things

- Rails
  - 5.2.2
- System dependencies
  - Mysql
    - 5.7
  - Node
    - 10.14.2
    - nodenv
      - `nodenv rehash`
  - Ruby
    - 2.6.5
    - rbenv
      - `rbenv rehash`
- Configuration
  - install mysql & build mysql2 gem in macOS >= Mojave
    - `brew install mysql@5.7`
    - bash
      - edit `~/.bash_profile`
      - ex: `export PATH=/usr/local/opt/mysql@5.7/bin/:$PATH`
    - fish
      - edit `~/.config/fish/config.fish`
      - ex: `set -U fish_user_paths "/usr/local/opt/mysql@5.7/bin" $fish_user_paths`
    - `brew install openssl`
    - `brew info openssl`
    - check the path for `LDFLAGS` => OPENSSL_LDFLAGS
      - ex: `-L/usr/local/opt/openssl@1.1/lib`
    - bash
      - `export LDFLAGS="{OPENSSL_LDFLAGS}"`
    - fish
      - `set -gx LDFLAGS "{OPENSSL_LDFLAGS}"`
    - `bundle config --local build.mysql2 --with-ldflags=$LDFLAGS`
    - `bundle install -j2`
  - start mysql server in macOS
    - `brew services start mysql@5.7`
    - `mysql_secure_installation`
      - validate password: n
      - new password: `password`
      - remove anonymous user: y
      - Disallow root login remotely: n
      - Remove test database: y
    - `brew services restart mysql@5.7`
- Database creation
  - `bin/rails db:create`
- Database initialization
  - `bin/rails db:migrate`
- How to run the test suite
  - WIP
- Services (job queues, cache servers, search engines, etc.)
  - WIP
- Deployment instructions
  - push to master then exec pipeline.
